from django.contrib.auth import get_user_model
from django.contrib.auth.forms import UserCreationForm
from django.forms import ModelForm


class UserForm(ModelForm):
    """Create a new user """

    class Meta:

        model = get_user_model()
        fields = "__all__"