# Generated by Django 3.1.7 on 2021-02-21 00:25

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0002_auto_20210221_0025'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='date',
            field=models.DateTimeField(default=datetime.datetime(2021, 2, 21, 0, 25, 12, 58042, tzinfo=utc)),
        ),
    ]
