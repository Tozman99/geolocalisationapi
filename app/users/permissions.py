from rest_framework.permissions import BasePermission
from rest_framework import permissions


class CustomIsOwnerAccess(BasePermission):
    """ Let the owner access its own data but not other"""

    def has_object_permission(self, request, view, obj):

        if request.method in permissions.SAFE_METHODS:
            
            return obj.owner == request.user
            breakpoint()
        return obj.owner == request.user
        