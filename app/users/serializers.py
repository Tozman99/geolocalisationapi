from rest_framework.serializers import ModelSerializer
from .models import User
from django.contrib.auth import get_user_model
from rest_framework import serializers
from django.utils.translation import gettext_lazy as _
from django.contrib.auth import authenticate


class UserSerializer(ModelSerializer):

    class Meta:
        model = User
        fields = ["id", "email", "password"]
        extra_kwargs = {
            "password": {"write_only": True}
        }

    def create(self, validated_data):
        """Create a new user"""
        return get_user_model().objects.create_user(**validated_data)

    def update(self, instance, validated_data):
        """update the user infos"""

        password = validated_data.pop("password", None)

        user = super().update(instance, validated_data)

        if password:
            user.set_password(password)
            user.save
        
        return user


class AuthTokenSerializer(serializers.Serializer):
    
    email = serializers.CharField(
        label = _("email"),
        write_only = True,
    )

    password = serializers.CharField(
        label=_("Password"),
        style={'input_type': 'password'},
        trim_whitespace=False,
        write_only=True
    )

    def validate(self, attrs):
        """Validate data"""

        email = attrs.get("email")
        password = attrs.get("password")

        if email and password:

            user = authenticate(request=self.context.get("request"),
                                username=email, password=password)
            
            if not user:
                
                msg = _("Unable to log with provided credentials ")
                raise serializers.ValidationError(msg, code='authorization')
        else:
            
            msg = _('Must include "username" and "password".')
            raise serializers.ValidationError(msg, code='authorization')
        
        attrs["user"] = user

        return attrs