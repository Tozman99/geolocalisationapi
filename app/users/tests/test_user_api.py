from rest_framework.test import APIClient
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token
from rest_framework import status
from rest_framework.test import APITestCase
from django.urls import reverse
from django.test import TestCase
from django.urls import reverse
from django.contrib.auth import get_user_model
from django.core import serializers
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token


USERS_URL = reverse("users:User-list-create")
CREATE_USER_URL = reverse("users:user-create")
TOKEN_URL = reverse("users:authtoken")
USER_ME = reverse("users:user-detail")

#def get_detail_user_url(pk):
 #   """Create the url for the detail page """
  #  return reverse("users:user-detail", args=[pk])


class UserPublicTests(TestCase):
    """ tests when the user perform get request """

    def setUp(self):
        """Set the client and the user to create """

        self.client = APIClient()
    
    def test_users_create_access(self):
        """Check if we can access the USERS_URL page / GET"""

        res = self.client.get(CREATE_USER_URL, format="json")
        self.assertEqual(res.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_users_successfully_created(self):

        user_infos = {
            "email": "testy@gmail.com",
            "password": "Test123"
        }
        res = self.client.post(CREATE_USER_URL, user_infos, format="json")
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)
        user = get_user_model().objects.get(**res.data)
        self.assertEqual(user.email, res.json()["email"])
        self.assertTrue(user.check_password(user_infos["password"]))
        self.assertNotIn("password", res.data)

    def test_user_already_exist(self):
        """Post request => Bad request if a user already exists"""
        user_infos = {
            "email": "testy@gmail.com",
            "password": "Test123"
        }
        user = get_user_model().objects.create_user(**user_infos)
        res = self.client.post(CREATE_USER_URL, user_infos, format="json")

        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
    
    def test_create_token(self):
        
        payload = {
            "email": "abcdfr@gmail.com",
            "password": "Test123"
        }

        user = get_user_model().objects.create_user(**payload)
        token = Token.objects.create(user=user)
        res = self.client.post(TOKEN_URL, payload, format="json")
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(res.data["token"], token.key)


class UserPrivateTest(TestCase):
    """ Class that can perform PUT/PATCH/DELETE action"""

    def setUp(self):

        self.client = APIClient()
        self.user = get_user_model().objects.create_user("dedef@gmail.com", "Test123")
        self.client.force_authenticate(user=self.user)

    def test_completly_update_user(self):
        """Update completly a user with PUT action """

        payload = {
            "email": "NewEmail@gmail.com",
            "password": "abcd12345$"
        }

        res = self.client.put(USER_ME, payload, format="json")

        self.assertEqual(res.status_code, status.HTTP_200_OK)
    
    def test_get_user_detail(self):
        """Get user data"""
        payload = {
            "email": "abcdfr@gmail.com",
            "password": "Test123"
        }
        user = get_user_model().objects.create_user(**payload)
        res = self.client.get(USER_ME, format="json")

        self.assertEqual(res.status_code, status.HTTP_200_OK)
    
    def test_partial_update_user(self):
        """ Update partially the user infos """

        payload = {
            "email": "dfrg@gmail.com"
        }
        res = self.client.patch(USER_ME, payload, format="json")
        self.user.refresh_from_db()
        breakpoint()

        self.assertEqual(res.status_code, status.HTTP_200_OK)