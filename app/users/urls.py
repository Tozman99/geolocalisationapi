from django.urls import path, include
from .views import home_view
from .views import (UserListCreateView,
                    UserCreateView, UserRetrieveView,
                        CustomAuthToken)

app_name = "users"

urlpatterns = [
    path("home/", home_view, name="home"),
    path("users/", UserListCreateView.as_view(), name="User-list-create"),
    path("create/", UserCreateView.as_view(), name="user-create"),
    path("user/me/", UserRetrieveView.as_view(), name="user-detail"),
    path("user/token/", CustomAuthToken.as_view(), name="authtoken")
]
