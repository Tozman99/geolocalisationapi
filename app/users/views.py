from django.shortcuts import render
from django.http import HttpResponse
from rest_framework.generics import (ListCreateAPIView, CreateAPIView,
                                        RetrieveUpdateAPIView)
from .models import User
from .serializers import UserSerializer, AuthTokenSerializer
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework.authentication import TokenAuthentication
from rest_framework.settings import api_settings
from .permissions import CustomIsOwnerAccess
from django.shortcuts import get_object_or_404
from .forms import UserForm
# Create your views here.


def home_view(request):

    return HttpResponse("Home View")

class UserListCreateView(ListCreateAPIView):

    queryset = User.objects.all()
    serializer_class = UserSerializer

class UserCreateView(CreateAPIView):
    
    queryset = User.objects.all()
    serializer_class = UserSerializer

class UserRetrieveView(RetrieveUpdateAPIView):
    
    serializer_class = UserSerializer
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_object(self):
        """ Return the user with tokenAuthentification """
        
        return self.request.user


class CustomAuthToken(ObtainAuthToken):
    """ obtain token """
    serializer_class = AuthTokenSerializer
    renderer_classes = api_settings.DEFAULT_RENDERER_CLASSES


    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created = Token.objects.get_or_create(user=user)
        
        return Response({'token': token.key})


def UserFormView(request):

    return render(request, "registration.html", {"form": UserForm})

